
CREATE TABLE default.test_table (
`id` UInt16,
 `name` String,
 `value` String,
 `create_date` Date,
 `array` Array(String)
) ENGINE = MergeTree(create_date, id, 8192)


INSERT INTO `default`.test_table (id,name,value,create_date,`array`) VALUES (
2,'testNama','testValue','2019-12-30',['__name__=ecs_cpu_util','timestamp=1577419320000']);
