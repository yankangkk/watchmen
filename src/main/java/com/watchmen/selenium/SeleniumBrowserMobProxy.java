package com.watchmen.selenium;

import java.util.List;

import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpResponse;
import net.lightbody.bmp.BrowserMobProxy;
import net.lightbody.bmp.BrowserMobProxyServer;
import net.lightbody.bmp.client.ClientUtil;
import net.lightbody.bmp.core.har.Har;
import net.lightbody.bmp.core.har.HarEntry;
import net.lightbody.bmp.core.har.HarNameValuePair;
import net.lightbody.bmp.core.har.HarRequest;
import net.lightbody.bmp.core.har.HarResponse;
import net.lightbody.bmp.filters.RequestFilter;
import net.lightbody.bmp.proxy.CaptureType;
import net.lightbody.bmp.util.HttpMessageContents;
import net.lightbody.bmp.util.HttpMessageInfo;

/**
 * 
 * @author kk
 * @Description selenium使用browserMobProxy代理
 */
public class SeleniumBrowserMobProxy {

	public static void main(String[] args) {
		String webDriverDir = "浏览器驱动路径";
		// 加载驱动
		System.setProperty("webdriver.chrome.driver", webDriverDir);
		BrowserMobProxy browserMobProxy = new BrowserMobProxyServer();
		browserMobProxy.start();
		browserMobProxy.enableHarCaptureTypes(CaptureType.REQUEST_CONTENT, CaptureType.RESPONSE_CONTENT);
		browserMobProxy.setHarCaptureTypes(CaptureType.RESPONSE_CONTENT);
		browserMobProxy.newHar("kk");

		Proxy seleniumProxy = ClientUtil.createSeleniumProxy(browserMobProxy);
		// 设置浏览器参数
		ChromeOptions options = new ChromeOptions();
		options.setProxy(seleniumProxy);
		options.setAcceptInsecureCerts(true);
		options.setExperimentalOption("useAutomationExtension", false);
		// 创建驱动对象
		WebDriver driver = new ChromeDriver(options);

		// 监听网络请求
		browserMobProxy.addRequestFilter(new RequestFilter() {
			@Override
			public HttpResponse filterRequest(HttpRequest request, HttpMessageContents contents,
					HttpMessageInfo messageInfo) {
				// 打印浏览器请求的url和请求头
				System.out.println(request.getUri() + " --->> " + request.headers().get("Cookie"));
				return null;
			}
		});

		// 打开链接
		driver.get("https://www.baidu.com/");

		// 获取返回的请求内容
		Har har = browserMobProxy.getHar();
		List<HarEntry> entries = har.getLog().getEntries();
		for (HarEntry harEntry : entries) {
			HarResponse response = harEntry.getResponse();
			HarRequest request = harEntry.getRequest();
			String url = harEntry.getRequest().getUrl();
			List<HarNameValuePair> headers = request.getHeaders();
			for (HarNameValuePair harp : headers) {
				System.out.println(harp.toString());
			}
		}

	}
}
