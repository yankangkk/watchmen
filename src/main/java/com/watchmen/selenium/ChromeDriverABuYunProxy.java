package com.watchmen.selenium;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

/**
 * 
 * @author kk
 * @Description selenium使用阿布云代理
 */
public class ChromeDriverABuYunProxy {

	
	public static void main(String[] args) {
		try {
			// 加载驱动
			System.setProperty("webdriver.chrome.driver", "浏览器驱动存放路径");
			// 设置浏览器参数
			ChromeOptions options = new ChromeOptions();
			// ip代理
			options.addExtensions(new File("插件脚本存放的路径"));
			// 创建驱动对象
			WebDriver driver = new ChromeDriver(options);
			// 使用ip138验证代理是否生效
			driver.get("https://www.ip138.com/");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
