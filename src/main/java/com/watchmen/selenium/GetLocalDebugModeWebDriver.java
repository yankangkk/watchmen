package com.watchmen.selenium;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import cn.hutool.core.util.RuntimeUtil;

/**
 * @author kk
 * @Description 获取本地调试模式浏览器驱动
 */
public class GetLocalDebugModeWebDriver {

	public static void main(String[] args) {
		ChromeDriver driver = null;
		// 调用cmd命令行，启动本地安装的谷歌浏览器
		String command = "cmd /c start chrome --remote-debugging-port=9222";
		RuntimeUtil.execForStr(command);
		// 加载驱动
		System.setProperty("webdriver.chrome.driver", "浏览驱动路径");
		ChromeOptions option = new ChromeOptions();
		option.setExperimentalOption("debuggerAddress", "127.0.0.1:9222");
		driver = new ChromeDriver(option);
		driver.get("https://www.baidu.com/");
	}
}
