package com.watchmen.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.watchmen.clickhouse.entity.TestTableEntity;
import com.watchmen.clickhouse.service.TestTableService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * 
 * @author kk
 * @Description Clickhouse增删改查测试路由
 */
@Api(tags =  "clickhouse增删改查测试路由")
@RestController
@RequestMapping("/clickhouse")
public class ClickhouseTestController {
	
	@Autowired
	TestTableService testTableService; 
	
	@ApiOperation(value = "clickhouse按id删除数据", notes = "删除测试", httpMethod = "DELETE")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "id", dataType = "Integer"),
    })
	@DeleteMapping
	public void delete(@RequestParam("id") Integer id) {
		testTableService.deleteById(id);
	}
	
	
	
	/**
	 * 分页查询
	 * @return
	 */
	@ApiOperation(value =  "clickhouse分页测试", notes = "分页测试" ,httpMethod = "GET")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "page", value = "第几页", dataType = "Integer"),
		@ApiImplicitParam(name = "page_size", value = "每页大小", dataType = "Integer")
	})
	@GetMapping("/list")
	public Object list(@RequestParam(value = "page",defaultValue = "1") Integer page,
					   @RequestParam(value = "page_size",defaultValue = "10") Integer pageSize) {
		List<TestTableEntity> list = testTableService.list();
		System.out.println(list);
		return testTableService.list(page, pageSize);
	}
	
	
}
