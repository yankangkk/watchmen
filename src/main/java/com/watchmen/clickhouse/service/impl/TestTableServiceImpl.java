package com.watchmen.clickhouse.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.watchmen.clickhouse.entity.TestTableEntity;
import com.watchmen.clickhouse.mapper.TestTableMapper;
import com.watchmen.clickhouse.service.TestTableService;

@Service
public class TestTableServiceImpl extends ServiceImpl<TestTableMapper,TestTableEntity> implements TestTableService {

	@Autowired
	private TestTableMapper testTableMapper;
	
	@Override
	public Page<TestTableEntity> list(Integer page, Integer pageSize) {
		//return this.page(new Page<TestTableEntity>(page,pageSize),new QueryWrapper<TestTableEntity>());
		Page<TestTableEntity> pages = new Page<>();
		pages.setRecords(testTableMapper.selectPages(page, pageSize));
		return pages;
	}
	
	/**
	 * 按id删除
	 * @param id
	 */
	@Override
	public void deleteById(Integer id) {
		testTableMapper.deleteById(id);
	}
}
