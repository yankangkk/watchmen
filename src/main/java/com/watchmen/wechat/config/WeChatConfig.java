package com.watchmen.wechat.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import lombok.Data;

/**
 * @author kk
 * @Description 微信配置类
 */
@Component
@PropertySource({"classpath:/config/config.properties"})
@ConfigurationProperties
@Data
public class WeChatConfig {
	
	
	@Value("${appID}")
	private String appID;

	
	@Value("${appSecret}")
	private String appSecret;

	// 微信支付的API密钥 
	@Value("${weChatApiKey}")
	private String weChatApiKey;

	// 微信支付的商户号 
	@Value("${weChatMerchant}")
	private String weChatMerchant;
	
	// 微信统一下单API地址 
	@Value("https://api.mch.weixin.qq.com/pay/unifiedorder")
	private String weChatOrderUrl;
	
	// 获取access_token，url
	@Value("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET")
	String accessTokenUrl;
	
	// 使用access_token 获取到jsapi_ticket，url
	@Value("https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=ACCESS&type=jsapi")
	String ticketUrl;

	// 微信模板消息推送，url
	@Value("https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=TOKEN")
	String industryUrl;

	@Value("${weChatCallbackUrl}")
	private String weChatCallbackUrl;

	@Value("${weChatPay}")
	private String weChatPay;

	// 加密方式
	@Value("MD5")
	private String weChatMD5Sign;
	
	@Value("watchmen")
	private String body;

	@Value("NATIVE")
	private String tradeTypeNative;
	
	@Value("JSAPI")
	private String tradeTypeJSAPI;
	
	// 微信模板消息id（需要到微信公众号配置）
	@Value("TemplateId")
	private String TemplateId;
	
	
}
