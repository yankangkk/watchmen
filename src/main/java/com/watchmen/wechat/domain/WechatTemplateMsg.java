package com.watchmen.wechat.domain;

import java.util.TreeMap;

import lombok.Data;

/**
 * @author kk
 * @Description 模板消息 domain
 */
@Data
public class WechatTemplateMsg {
	
	private String touser; //接收者openid
	private String templateId; //模板ID

	private String url; //模板跳转链接
	
	private TreeMap<String, TreeMap<String, String>> data; //data数据
	
	
	/**
	 * 参数
	 * @param value
	 * @param color 可不填
	 * @return
	 */
	public static TreeMap<String, String> item(String value, String color) {
		TreeMap<String, String> params = new TreeMap<String, String>();
		params.put("value", value);
		params.put("color", color);
		return params;
	}

}
