package com.watchmen.wechat.token;


import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Maps;
import com.watchmen.wechat.config.WeChatConfig;
import com.watchmen.wechat.util.HttpRequest;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * @author kk
 * @Description 微信JS SDK, access_token, ticket
 */
@Slf4j
@Component
public class WechatVoucher {

	
	@Autowired
	WeChatConfig weChatConfig;
	
	@Autowired
	HttpRequest httpRequest;
	
	// 本地缓存
	public static LoadingCache<String, String> caches = CacheBuilder
				                                             .newBuilder()
				                                              // 设置过期时间
				                                             .expireAfterWrite(7180000, TimeUnit.SECONDS)
				                                             .build(new CacheLoader<String, String>() {
																
				                                                @Override
																public String load(String key) throws Exception {
																	return null;
																}
				                                             });
	
	
	public Map<String, String> voucher(){
		Map<String, String> dataMap = Maps.newLinkedHashMap();
		// 缓存access_token
		String cacheAccess = "";
				
		// 缓存jsapi_ticket
        String cacheTicket = "";
        
        String accessTokenUrl = weChatConfig.getAccessTokenUrl().replace("APPID", weChatConfig.getAppID())
                												.replace("APPSECRET", weChatConfig.getAppSecret());
		
        log.info("accessTokenUrl是:{}",accessTokenUrl);
        
        
        
        // 获取access_token,并放入缓存
     	try {
     		// 有则赋值，没有则调用微信接口获取access_token在赋值
     		cacheAccess = caches.get("access_token");
     	} catch (Exception e) {
     		try {
     			// 发送请求，获取access_token
     			String accessData = httpRequest.doHttpsJson_HttpClient(accessTokenUrl,"POST",null);
     			
     			JSONObject parseObj = JSONUtil.parseObj(accessData);
     			 
     			String accessToken = parseObj.getStr("access_token");
     			
     			// 放入缓存
     			caches.put("access_token", accessToken);
     			cacheAccess = caches.get("access_token");
     		} catch (Exception e2) {
     			e2.printStackTrace();
     		}
     	}
     	
     	log.info("cache_access值是:{}",cacheAccess);
     	
     	
     	
     	//拿到了access_token 使用access_token 获取到jsapi_ticket
     	String ticketUrl = weChatConfig.getTicketUrl().replace("ACCESS", cacheAccess);
     	
     	
        // 获取jsapi_ticket,并放入缓存
     	try {
     		// 有则赋值，没有则调用微信接口获取access_token在赋值
     		cacheTicket = caches.get("jsapi_ticket");
     	} catch (Exception e) {
     		try {
     			//调用jsapi_ticket_url 请求拿到jsapi_ticket
     			String ticketData = httpRequest.doHttpsJson_HttpClient(ticketUrl,"POST",null);
     			
     			JSONObject parseObj = JSONUtil.parseObj(ticketData);
     			
     			String jsapiTicket = parseObj.getStr("ticket");
     			// 放入缓存
     			caches.put("jsapi_ticket", jsapiTicket);
     			cacheTicket = caches.get("jsapi_ticket");
     		} catch (Exception e2) {
     			e2.printStackTrace();
     		}
     	}
     	
     	
     	log.info("cache_ticket{值是:{}",cacheTicket);
     	
     	// 返回结果
     	dataMap.put("ticket", cacheTicket);
     	dataMap.put("accessTtoken", cacheAccess);
     		
		return dataMap;
	}
	
}
